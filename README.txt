Commerce trade price
===============

CONTENTS OF THIS FILE
---------------------

  * Introduction
  * Requirements
  * Installation
  * Configuration
  * Author
  * Similar projects and how they are different

INTRODUCTION
------------
This module provides trade price for  Drupal Commerce.
Provides:
 - price resolver
 - views fields for editing trade price and quantity

REQUIREMENTS
------------

Commerce price
Views

INSTALLATION
------------

Install module as usual via Drupal UI, Drush or Composer.

CONFIGURATION
----------------

1. Go to /admin/commerce/config/product-variation-types/default/edit/fields
Add field with machine name "field_trade_price"
Add field with machine name "field_trade_price_quantity"
2. Go to  Manage display /admin/commerce/config/product-variation-types/default/edit/display and set output format of field_trade_price to Calculated price
3. Go to Manage form display /admin/commerce/config/product-variation-types/default/edit/form-display and ser field order as you like
4. Go to /admin/commerce/products ->  edit view -> Add field Trade price text field and Trade quantity text field
5. At product template add this block
```
{% if product.variation_field_trade_price.0 and  product.variation_field_trade_price_quantity.0 %}
<div class="prod-price-trade">
	<div class="prod-price-trade__title">{{ 'Trade price'|trans }}</div>
	<div class="prod-price-trade__price">{{- 'From'|trans -}}{{- product.variation_field_trade_price_quantity -}} {{- 'qty.'|trans }} &ndash; {{ display_trade_price(product.variation_field_trade_price)|commerce_price_format }}</div>
</div>
{% endif %}
6. Enjoy)
```
AUTHOR
------

shmel210
Drupal: (https://www.drupal.org/user/2600028)
Email: shmel210@zina.com.ua

Company: Zina Design Studio
Website: (https://www.zina.design)
Drupal: (https://www.drupal.org/user/361734/)
Email: info@zina.design

SIMILAR PROJECTS AND HOW THEY ARE DIFFERENT
-------------------------------------------
There is no similar projects at this time.
