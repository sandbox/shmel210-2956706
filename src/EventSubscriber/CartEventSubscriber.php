<?php

namespace Drupal\commerce_trade_price\EventSubscriber;

use Drupal\commerce_cart\Event\CartOrderItemUpdateEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Drupal\commerce_cart\Event\CartEntityAddEvent;
use Drupal\commerce_cart\Event\CartEvents;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\StringTranslation\TranslationInterface;
use Drupal\Core\Link;

class CartEventSubscriber implements EventSubscriberInterface {

  use StringTranslationTrait;

  /**
   * Constructs a new CartEventSubscriber object.
   *
   * @param \Drupal\Core\StringTranslation\TranslationInterface $string_translation
   *   The string translation.
   */
  public function __construct(TranslationInterface $string_translation) {
    $this->stringTranslation = $string_translation;
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    $events = [
      CartEvents::CART_ORDER_ITEM_UPDATE => 'calculatePrice',
    ];
    return $events;
  }

  public function calculatePrice(CartOrderItemUpdateEvent $event) {
    $order_item = $event->getOrderItem();
    /** @var \Drupal\commerce_product\Entity\ProductVariation $product_variation */
    $product_variation = $order_item->getPurchasedEntity();
//    $product_variation->get('field_trade_price')->resolve();
  }

}
