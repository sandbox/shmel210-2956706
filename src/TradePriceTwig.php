<?php
namespace Drupal\commerce_trade_price;

/**
 * Class DefaultService.
 *
 * @package Drupal\commerce_trade_price
 */
class TradePriceTwig extends \Twig_Extension {

  /**
   * {@inheritdoc}
   * This function must return the name of the extension. It must be unique.
   */
  public function getName() {
    return 'display_trade_price';
  }

  /**
   * In this function we can declare the extension function
   */
  public function getFunctions() {
    return array(
      new \Twig_SimpleFunction('display_trade_price',
          array($this, 'display_trade_price')
      )
    );
  }

  public function display_trade_price($field_trade_price) {
    if(!isset($field_trade_price['#items'])) {
      return '';
    }
    /** @var \Drupal\Core\Field\FieldItemList $items */
    $items = $field_trade_price['#items'];
    if($items->isEmpty()) {
      return '';
    }
    else {
      /** @var \Drupal\commerce_price\Plugin\Field\FieldType\PriceItem $price_item */
      $price_item = $items->first();
      return $price_item->toPrice();
    }
  }

}
