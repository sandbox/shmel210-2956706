<?php

namespace Drupal\commerce_trade_price\Resolvers;

use Drupal\commerce\Context;
use Drupal\commerce\PurchasableEntityInterface;
use Drupal\commerce_price\Resolver\PriceResolverInterface;

/**
 * Class TradePriceResolver.
 *
 * @package Drupal\commece_trade_price\Resolvers
 */
class TradePriceResolver implements PriceResolverInterface {

  /**
   * {@inheritdoc}
   */
  public function resolve(PurchasableEntityInterface $entity, $quantity, Context $context) {
    // Make sure that product variation has a field called Saleprice.
    if (!$entity->hasField('field_trade_price') || !$entity->hasField('field_trade_price_quantity')) {
      return NULL;
    }
    $field_trade_price = $entity->get('field_trade_price');
    $field_trade_price_quantity = $entity->get('field_trade_price_quantity');

    if ($field_trade_price->isEmpty() || $field_trade_price_quantity->isEmpty()) {
      return NULL;
    }
    /** @var \Drupal\commerce_price\Price $trade_price */
    $trade_price = $field_trade_price->first()->toPrice();
    $trade_price_quantity = $field_trade_price_quantity->first()->getValue()['value'];
    if (!$trade_price->getNumber() || !$trade_price_quantity || $trade_price_quantity > $quantity) {
      return NULL;
    }


    return $trade_price;
  }

}