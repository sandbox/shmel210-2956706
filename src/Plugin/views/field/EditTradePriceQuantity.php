<?php

namespace Drupal\commerce_trade_price\Plugin\views\field;

use Drupal\Core\Form\FormStateInterface;
use Drupal\views\Plugin\views\field\FieldPluginBase;
use Drupal\views\Plugin\views\field\UncacheableFieldHandlerTrait;
use Drupal\views\ResultRow;

/**
 * Defines a form element for editing the order item quantity.
 *
 * @ViewsField("commerce_trade_price_edit_trade_price_quantity")
 */
class EditTradePriceQuantity extends FieldPluginBase {

  use UncacheableFieldHandlerTrait;

  /**
   * {@inheritdoc}
   */
  public function clickSortable() {
    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  protected function defineOptions() {
    $options = parent::defineOptions();
    return $options;
  }


  /**
   * {@inheritdoc}
   */
  public function getValue(ResultRow $row, $field = NULL) {
    return '<!--form-item-' . $this->options['id'] . '--' . $row->index . '-->';
  }

  /**
   * Form constructor for the views form.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   */
  public function viewsForm(array &$form, FormStateInterface $form_state) {
    // Make sure we do not accidentally cache this form.
    $form['#cache']['max-age'] = 0;
    // The view is empty, abort.
    if (empty($this->view->result)) {
      return;
    }
    $form[$this->options['id']]['#tree'] = TRUE;
    foreach ($this->view->result as $row_index => $row) {
      /** @var \Drupal\commerce_product\Entity\ProductVariationInterface $product_variation */
      $product_variation = $this->getEntity($row);
      if (!$product_variation->hasField('field_trade_price') || !$product_variation->hasField('field_trade_price_quantity')) {
        return;
      }
      $field_trade_price_quantity = $product_variation->get('field_trade_price_quantity');
      if ($field_trade_price_quantity->isEmpty()) {
        $form[$this->options['id']][$row_index] = [
          '#type' => 'textfield',
          '#title' => $this->t('Price'),
          '#title_display' => 'invisible',
          '#default_value' => '',
          '#size' => 9,
          '#ajax' => [
            'callback' => 'Drupal\commerce_trade_price\Plugin\views\field\EditTradePriceQuantity::viewsFormSubmit',
            'wrapper' => 'not-exist-element',
            'event' => 'change',
          ],
        ];
      }
      else {
        $trade_price_quantity = $field_trade_price_quantity->first()->getValue()['value'];
        $form[$this->options['id']][$row_index] = [
          '#type' => 'textfield',
          '#title' => $this->t('Price'),
          '#title_display' => 'invisible',
          '#default_value' => $trade_price_quantity,
          '#size' => 9,
          '#ajax' => [
            'callback' => 'Drupal\commerce_trade_price\Plugin\views\field\EditTradePriceQuantity::viewsFormSubmit',
            'wrapper' => 'not-exist-element',
            'event' => 'change',
          ],
        ];
      }
    }
  }

  /**
   * Submit handler for the views form.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   */
  public function viewsFormSubmit(array &$form, FormStateInterface $form_state) {
    $triggering_element = $form_state->getTriggeringElement();
    $view = reset($form_state->getBuildInfo()['args']);
    $quantity_list = $form_state->getValue($triggering_element['#parents'][0], []);
    foreach ($quantity_list as $row_index => $quantity) {
      /** @var \Drupal\commerce_product\Entity\ProductVariationInterface $product_variation */
      $view_result = $view->result[$row_index];
      $product_variation = $view_result->_relationship_entities['variations'];
      $field_trade_price_quantity = $product_variation->get('field_trade_price_quantity');
      if ($field_trade_price_quantity->isEmpty() || $field_trade_price_quantity->first()->getValue()['value'] != $quantity) {
        $product_variation->set('field_trade_price_quantity', $quantity);
        $product_variation->save();
      }
    }
    return ['#markup' => ''];
  }

  /**
   * {@inheritdoc}
   */
  public function query() {
    // Do nothing.
  }

}
